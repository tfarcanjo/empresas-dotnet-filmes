﻿using Ioasys.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Mappings
{
    public class MovieMapping : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Title)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(m => m.Director)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(m => m.Genre)
                .HasColumnType("varchar(50)");

            builder.Property(m => m.Year)
                .HasColumnType("int");

            builder.Property(m => m.Rating)
                .HasColumnType("decimal(2,1)");

            builder.Property(m => m.TotalRatings)
                .HasColumnType("decimal(2,1)");

            builder.Property(m => m.TotalVotes)
                .HasColumnType("int");

            builder.ToTable("Movies");
        }
    }
}
