﻿using Ioasys.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Mappings
{
    public class MovieActorMapping : IEntityTypeConfiguration<MovieActor>
    {
        public void Configure(EntityTypeBuilder<MovieActor> builder)
        {
            builder.HasKey(x => new {x.MovieId, x.ActorId});

            builder.HasOne(bc => bc.Movie)
                .WithMany(b => b.MovieActors)
                .HasForeignKey(bc => bc.MovieId);

            builder.HasOne(bc => bc.Actor)
                .WithMany(b => b.MovieActors)
                .HasForeignKey(bc => bc.ActorId);

            builder.ToTable("MovieActors");
        }
    }
}
