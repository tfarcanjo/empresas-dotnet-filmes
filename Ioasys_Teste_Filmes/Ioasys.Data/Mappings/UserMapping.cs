﻿using Ioasys.Business.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Mappings
{
    public class UserMapping : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(u => u.Id);

            builder.Property(u => u.Name)
                .IsRequired()
                .HasColumnType("varchar(100)");


            builder.Property(u => u.Email)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(u => u.Password)
                .IsRequired()
                .HasColumnType("varchar(100)");

            builder.Property(u => u.Role)
                .IsRequired()
                .HasColumnType("varchar(30)");

            builder.Property(u => u.Active)
                .IsRequired()
                .HasColumnType("bit");

            builder.ToTable("Users");
        }
    }
}
