﻿using Ioasys.Business.Interfaces;
using Ioasys.Business.Models;
using Ioasys.Data.Context;

namespace Ioasys.Data.Repositories
{
    public class ActorRepository : BaseRepository<Actor>, IActorRepository
    {
        public ActorRepository(MoviesDbContext context) : base(context) { }
    }
}
