﻿using Ioasys.Business.Interfaces;
using Ioasys.Business.Models;
using Ioasys.Data.Context;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Data.Repositories
{
    public class MovieActorRepository : BaseRepository<MovieActor>, IMovieActorRepository
    {
        public MovieActorRepository(MoviesDbContext context) : base(context) { }

        public async Task<IEnumerable<MovieActor>> GetMoviesWithActors()
        {
            return await Db.MovieActors
                                .Include(m => m.Movie)
                                .Include(m => m.Actor).ToListAsync();

        }

    }
}
