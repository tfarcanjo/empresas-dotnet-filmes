﻿using Ioasys.Business.Interfaces;
using Ioasys.Business.Models;
using Ioasys.Data.Context;

namespace Ioasys.Data.Repositories
{
    public class MovieRepository : BaseRepository<Movie>, IMovieRepository
    {
        public MovieRepository(MoviesDbContext context) : base(context)
        {

        }

    }
}
