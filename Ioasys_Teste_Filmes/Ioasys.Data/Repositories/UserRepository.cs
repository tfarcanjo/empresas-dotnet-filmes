﻿using Ioasys.Business.Interfaces;
using Ioasys.Business.Models;
using Ioasys.Data.Context;

namespace Ioasys.Data.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(MoviesDbContext context) : base(context) { }

    }
}
