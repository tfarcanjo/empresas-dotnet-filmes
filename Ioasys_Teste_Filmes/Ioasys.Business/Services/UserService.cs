﻿using AutoMapper;
using Ioasys.Business.Dtos;
using Ioasys.Business.Interfaces;
using Ioasys.Business.Models;
using Ioasys.Business.Services.Interfaces;
using Ioasys.Business.Util;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<DtoUserOutput> GetUser(DtoUserAuth dtoUserAuth)
        {
            var hashedPassword = HashService.GetHash(dtoUserAuth.Password);

            var user = (await _userRepository
                .Find(u => u.Email.ToLower() == dtoUserAuth.Email.ToLower() &&
                        u.Password == hashedPassword))
                .FirstOrDefault();

            if (user != null)
            {

                var dtoUserOutput = new DtoUserOutput
                {
                    User = _mapper.Map<DtoUser>(user)
                };

                return dtoUserOutput;
            }

            return new DtoUserOutput();
        }

        public async Task<long> RegisterUser(DtoUserRegistration dtoUser)
        {
            var userExist = _userRepository.Find(u => u.Name.ToLower() == dtoUser.Name.ToLower()
                                                 && u.Email.ToLower() == dtoUser.Email.ToLower())
                                            .Result.Any();

            if (userExist)
                return 0;

            var user = _mapper.Map<User>(dtoUser);
            user.Password = HashService.GetHash(user.Password);
            user.Active = true;

            await _userRepository.Add(user);

            return user.Id;
        }

        public async Task<bool> ExcludeUser(long userId)
        {
            var user = (await _userRepository.Find(u => u.Id == userId)).FirstOrDefault();

            if (user == null)
                return false;

            user.Active = false;
            await _userRepository.Update(user);

            return true;
        }

        public async Task<bool> EditUser(DtoUserRegistration dtoUser)
        {
            var user = (await _userRepository.Find(u => u.Id == dtoUser.Id)).FirstOrDefault();

            if (user != null)
            {
                user.Name = dtoUser.Name;
                user.Email = dtoUser.Email;
                user.Password = HashService.GetHash(dtoUser.Password);
                user.Role = dtoUser.Role;

                await _userRepository.Update(user);
                return true;
            }

            return false;
        }

        public async Task<IEnumerable<DtoUser>> ListActiveNonAdminUsers(int page)
        {
            var users = (await _userRepository.GetAll())
                .Where(u => u.Role == "usuario")
                .OrderBy(u => u.Name).ToList();

            var dtoUsers = _mapper.Map<IEnumerable<DtoUser>>(users);
            PaginationOptions.Count = dtoUsers.Count();

            if (dtoUsers.Count() > PaginationOptions.PageSize)
                dtoUsers = Pagination<DtoUser>.Paginate(dtoUsers, page);

            return dtoUsers;
        }
    }
}
