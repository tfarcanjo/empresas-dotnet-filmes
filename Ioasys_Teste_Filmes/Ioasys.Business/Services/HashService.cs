﻿using System.Security.Cryptography;
using System.Text;

namespace Ioasys.Business.Services
{
    public static class HashService
    {
        public static string GetHash(string input)
        {

            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] data = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

                var sBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                return sBuilder.ToString();
            }


        }
    }
}
