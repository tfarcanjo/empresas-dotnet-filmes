﻿using AutoMapper;
using Ioasys.Business.Dtos;
using Ioasys.Business.Helper;
using Ioasys.Business.Interfaces;
using Ioasys.Business.Models;
using Ioasys.Business.Util;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Ioasys.Business.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IMovieActorRepository _movieActorRepository;
        private readonly IMapper _mapper;

        public MovieService(IMovieRepository movieRepository,
                            IActorRepository actorRepository,
                            IMovieActorRepository movieActorRepository,
                            IMapper mapper)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _movieActorRepository = movieActorRepository;
            _mapper = mapper;
        }

        public async Task<long> RegisterMovie(DtoMovie dtoMovie)
        {
            var movieDatabase = (await _movieRepository
                                .Find(m => m.Title == dtoMovie.Title))
                                .FirstOrDefault();


            if (movieDatabase != null)
                return 0;

            var movie = _mapper.Map<Movie>(dtoMovie);
            var movieActor = new MovieActor();

            await _movieRepository.Add(movie);

            var actors = _actorRepository.GetAll().Result.ToList();

            foreach (var actor in dtoMovie.Actors)
            {
                if (actors.Select(a => a.Name).Contains(actor))
                    movieActor.ActorId = actors.First(a => a.Name == actor).Id;                
                else
                {
                    var newActor = new Actor { Name = actor };
                    await _actorRepository.Add(newActor);
                    movieActor.ActorId = newActor.Id;
                }

                movieActor.MovieId = movie.Id;
                await _movieActorRepository.Add(movieActor);

            }

            return movie.Id;
        }

        public async Task<IEnumerable<DtoMovie>> ListMovies(int page)
        {
            var moviesActors = await _movieActorRepository.GetMoviesWithActors();

            var movies = moviesActors.Select(m => m.Movie).Distinct();

            var dtoMovies = _mapper.Map<IEnumerable<DtoMovie>>(movies);
            PaginationOptions.Count = dtoMovies.Count();

            foreach (var movie in dtoMovies)
            {
                movie.Actors = moviesActors
                    .Where(m => m.MovieId == movie.Id)
                    .Select(a => a.Actor.Name).ToList();

            }

            if (dtoMovies.Count() > PaginationOptions.PageSize)
                dtoMovies = Pagination<DtoMovie>.Paginate(dtoMovies, page);
            

            return dtoMovies.OrderByDescending(m => m.Rating).ThenBy(m => m.Title);
        }

        public async Task<StatusOperation> RateMovie(long movieId, DtoMovieRate dtoMovieRate)
        {
            if (dtoMovieRate.MovieRate < 0 || dtoMovieRate.MovieRate > 4)
                return new StatusOperation{ Status = (int)HttpStatusCode.BadRequest, 
                                            Message = @"Ocorreu um erro ao votar no filme selecionado 
                                                        Votos não podem ser maiores que 4 e nem menores que 0" };

            var movie = await _movieRepository.GetById(movieId);

            if(movie != null)
            {
                movie.TotalRatings += dtoMovieRate.MovieRate;
                movie.TotalVotes += 1;
                movie.Rating = movie.TotalRatings / movie.TotalVotes;
                await _movieRepository.Update(movie);
                return new StatusOperation { Status = (int)HttpStatusCode.OK, 
                                             Message = "Filme votado com sucesso" };
            }

            return new StatusOperation {Status = (int)HttpStatusCode.NotFound, 
                                        Message = "Filme não encontrado"};
        }
    }
}
