﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Business.Helper
{
    public class StatusOperation
    {
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
