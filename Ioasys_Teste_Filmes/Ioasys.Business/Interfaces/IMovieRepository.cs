﻿using Ioasys.Business.Models;

namespace Ioasys.Business.Interfaces
{
    public interface IMovieRepository : IBaseRepository<Movie>
    {

    }
}
