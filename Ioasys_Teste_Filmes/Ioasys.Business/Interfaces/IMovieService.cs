﻿using Ioasys.Business.Dtos;
using Ioasys.Business.Helper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Business.Interfaces
{
    public interface IMovieService
    {
        Task<long> RegisterMovie(DtoMovie dtoMovie);
        Task<IEnumerable<DtoMovie>> ListMovies(int page);
        Task<StatusOperation> RateMovie(long movieId, DtoMovieRate dtoMovieRate);
    }
}
