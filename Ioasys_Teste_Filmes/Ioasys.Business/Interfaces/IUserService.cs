﻿using Ioasys.Business.Dtos;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Business.Services.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<DtoUser>> ListActiveNonAdminUsers(int page);
        Task<DtoUserOutput> GetUser(DtoUserAuth dtoUserAuth);
        Task<long> RegisterUser(DtoUserRegistration dtoUser);
        Task<bool> EditUser(DtoUserRegistration dtoUser);
        Task<bool> ExcludeUser(long userId);

    }
}
