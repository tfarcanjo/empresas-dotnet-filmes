﻿using Ioasys.Business.Models;

namespace Ioasys.Business.Interfaces
{
    public interface IActorRepository : IBaseRepository<Actor>
    {

    }
}
