﻿using Ioasys.Business.Models;

namespace Ioasys.Business.Interfaces
{
    public interface IUserRepository : IBaseRepository<User>
    {

    }
}
