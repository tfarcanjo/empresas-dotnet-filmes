﻿using Ioasys.Business.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Business.Interfaces
{
    public interface IMovieActorRepository : IBaseRepository<MovieActor>
    {
        Task<IEnumerable<MovieActor>> GetMoviesWithActors();
    }
}
