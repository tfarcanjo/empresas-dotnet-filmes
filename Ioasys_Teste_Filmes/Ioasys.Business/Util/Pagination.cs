﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ioasys.Business.Util
{
    public class Pagination<T>
    {
        public static IEnumerable<T> Paginate(IEnumerable<T> list, int pageNumber)
        {
            PaginationOptions.PageNumber = pageNumber > PaginationOptions.PageNumber ? pageNumber :
                                                         PaginationOptions.PageNumber;

            return list.Skip((PaginationOptions.PageNumber - 1) * PaginationOptions.PageSize)
                                 .Take(PaginationOptions.PageSize);
        }
    }
}
