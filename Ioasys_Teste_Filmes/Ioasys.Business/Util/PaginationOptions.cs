﻿using System;

namespace Ioasys.Business.Util
{
	public static class PaginationOptions
	{
		public static int PageNumber { get; set; } = 1;
		public static int Count { get; set; }
		public static int PageSize { get; set; } = 20;
		public static int TotalPages => (int) Math.Ceiling(Count / (double)PageSize);
		public static object GetPaginationMetaData()
		{
			return new
			{
				PageSize,
				PageNumber,
				Count,
				TotalPages
			};
		}
	}
}
