﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Business.Dtos
{
    public class DtoMovieRate
    {
        public decimal MovieRate { get; set; }
    }
}
