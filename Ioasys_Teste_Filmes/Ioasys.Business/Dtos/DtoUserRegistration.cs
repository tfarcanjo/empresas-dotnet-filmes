﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace Ioasys.Business.Dtos
{
    public class DtoUserRegistration
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "O campo Name precisa ser fornecido")]
        public string Name { get; set; }
        [Required(ErrorMessage = "O campo Email precisa ser fornecido")]
        public string Email { get; set; }
        [Required(ErrorMessage = "O campo Password precisa ser fornecido")]
        public string Password { get; set; }
        [Required(ErrorMessage = "O campo Role precisa ser fornecido")]
        public string Role { get; set; }
    }
}
