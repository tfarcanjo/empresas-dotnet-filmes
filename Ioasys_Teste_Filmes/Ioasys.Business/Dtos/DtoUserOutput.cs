﻿namespace Ioasys.Business.Dtos
{
    public class DtoUserOutput
    {
        public string Token { get; set; }
        public DtoUser User{ get; set; }
    }
}
