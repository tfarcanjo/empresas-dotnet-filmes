﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Business.Dtos
{
    public class DtoMovie
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public decimal Rating { get; set; }
        public List<string> Actors { get; set; }
    }
}
