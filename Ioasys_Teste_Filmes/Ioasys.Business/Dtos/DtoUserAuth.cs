﻿using System.ComponentModel.DataAnnotations;

namespace Ioasys.Business.Dtos
{
    public class DtoUserAuth
    {
        [Required(ErrorMessage ="Esse campo é obrigatório")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Esse campo é obrigatório")]
        public string Password { get; set; }
    }
}
