﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Business.Models
{
    public class Movie
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Director { get; set; }
        public string Genre { get; set; }
        public int Year { get; set; }
        public decimal Rating { get; set; }
        public decimal TotalRatings { get; set; }
        public int TotalVotes { get; set; }
        public virtual ICollection<MovieActor> MovieActors { get; set; }
    }
}
