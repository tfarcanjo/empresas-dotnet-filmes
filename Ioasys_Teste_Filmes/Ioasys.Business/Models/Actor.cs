﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ioasys.Business.Models
{
    public class Actor
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<MovieActor> MovieActors { get; set; }
    }
}
