﻿namespace Ioasys.Business.Models
{
    public class MovieActor
    {
        public long MovieId { get; set; }
        public long ActorId { get; set; }
        public virtual Movie Movie { get; set; }
        public virtual Actor Actor { get; set; }
    }
}
