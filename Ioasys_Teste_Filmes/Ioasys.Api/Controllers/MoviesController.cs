﻿using Ioasys.Business.Dtos;
using Ioasys.Business.Interfaces;
using Ioasys.Business.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Ioasys.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieService _movieService;
        public MoviesController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        /// <summary>
        /// Realiza o cadastro de um filme
        /// </summary>
        /// <param name="dtoMovie">Dto contendo dados para o cadastro de filme</param>
        /// <returns>
        /// Id do filme cadastrado 
        /// </returns>
        [HttpPost]
        [Authorize(Roles = "administrador")]
        public async Task<ActionResult> RegisterMovie(DtoMovie dtoMovie)
        {
            var movieRegisteredIndex = await _movieService.RegisterMovie(dtoMovie);

            if (movieRegisteredIndex > 0)
                return Created(nameof(RegisterMovie), new { movieId = movieRegisteredIndex });
            
            return BadRequest("Não foi possível cadastrar o filme pois o mesmo já existe na base de dados");
        }

        /// <summary>
        /// Retorna lista paginada de filmes 
        /// </summary>
        /// <param name="page">Número da página da lista de filmes retornados</param>
        /// <returns>
        /// Lista de filmes cadastrados
        /// </returns>
        [HttpGet]
        public async Task<IEnumerable<DtoMovie>> ListMovies([FromQuery] int page)
        {
            var movies = await _movieService.ListMovies(page);

            var metadata = PaginationOptions.GetPaginationMetaData();

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return movies;
        }

        /// <summary>
        /// Realiza operação de voto em um filme
        /// </summary>
        /// <param name="id">Id do filme</param>
        /// <param name="dtoMovieRate">Dto contendo dados da votação do filme</param>
        /// <returns>
        /// </returns>
        [HttpPost("{id:long}")]
        [Authorize(Roles = "usuario")]
        public async Task<ActionResult> RateMovie(long id, DtoMovieRate dtoMovieRate)
        {
            var movieRate = await _movieService.RateMovie(id, dtoMovieRate);

            if (movieRate.Status == (int)HttpStatusCode.NotFound)
                return NotFound(movieRate.Message);
            else if (movieRate.Status == (int)HttpStatusCode.BadRequest)
                return BadRequest(movieRate.Message);

            return Ok(movieRate.Message);
        }

    }
}