﻿using Ioasys.Business.Dtos;
using Ioasys.Business.Services.Interfaces;
using Ioasys.Business.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Realiza cadastro de um usuário
        /// </summary>
        /// <param name="dtoUser">Dto contendo dados para o cadastro de usuário</param>
        /// <returns>
        /// Id do usuário cadastrado
        /// </returns>
        [HttpPost]
        public async Task<ActionResult> RegisterUser([FromBody] DtoUserRegistration dtoUser)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userRegisteredIndex = await _userService.RegisterUser(dtoUser);

            if (userRegisteredIndex > 0)
                return Created(nameof(RegisterUser), new { UserId = userRegisteredIndex });
            

            return BadRequest("Não foi possível cadastrar o usuário pois o mesmo já consta na base de dados");
        }

        /// <summary>
        /// Realiza a edição de um usuário
        /// </summary>
        /// <param name="id">Id do usuário a ser editado</param>
        /// <param name="dtoUser">Dto contendo dados do usuário a ser editado</param>
        /// <returns>
        /// Informações informações do usuário atualizado
        /// </returns>
        [HttpPut("{id:long}")]
        [Authorize(Roles = "administrador,usuario")]
        public async Task<ActionResult<DtoUserRegistration>> EditUser(long id, [FromBody] DtoUserRegistration dtoUser)
        {
            if (id != dtoUser.Id)
                return BadRequest("Id fornecido não confere com o do usuário");

            var userUpdated = await _userService.EditUser(dtoUser);
            dtoUser.Password = "";

            if (userUpdated) return Ok(dtoUser);

            return NoContent();
        }

        /// <summary>
        /// Realiza a exclusão lógica de um usuário
        /// </summary>
        /// <param name="id">Id do usuário que será excluído</param>
        /// <returns></returns>
        [HttpDelete("{id:long}")]
        [Authorize(Roles = "administrador,usuario")]
        public async Task<ActionResult> ExcludeUser(long id)
        {
            var userDeleted = await _userService.ExcludeUser(id);

            if (userDeleted)
                return Ok();

            return NoContent();
        }

        /// <summary>
        /// Realiza a listagem de usuários que não possuem o status de administrador
        /// </summary>
        /// <param name="page">Número da página da lista de usuários não administradores retornados</param>
        /// <returns>
        /// Lista de usuários que não possuem o status de administrador
        /// </returns>
        [HttpGet]
        [Authorize(Roles = "administrador")]
        public async Task<IEnumerable<DtoUser>> ListActiveNonAdminUsers([FromQuery] int page)
        {
            var users = await _userService.ListActiveNonAdminUsers(page);

            var metadata = PaginationOptions.GetPaginationMetaData();

            Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

            return users;
        }

    }
}