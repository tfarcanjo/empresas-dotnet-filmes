﻿using Ioasys.Business.Dtos;
using Ioasys.Business.Services;
using Ioasys.Business.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Ioasys.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly IUserService _userService;

        public LoginController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Realiza login de um usuário
        /// </summary>
        /// <param name="dtoUserAuth">Dto contendo dados para efetuação de login</param>
        /// <returns>
        ///   Retorna um usuário e seu token de acesso
        /// </returns>
        [HttpPost]
        public async Task<ActionResult<DtoUserOutput>> Login([FromBody] DtoUserAuth dtoUserAuth)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var userOutPut = await _userService.GetUser(dtoUserAuth);

            if (userOutPut.User == null)
                return BadRequest(new { message = "Usuário e/ou senha inválidos" });

            var token = TokenService.GenerateToken(userOutPut.User.Email, userOutPut.User.Role);

            userOutPut.Token = token;

            return userOutPut;
        }

    }
}