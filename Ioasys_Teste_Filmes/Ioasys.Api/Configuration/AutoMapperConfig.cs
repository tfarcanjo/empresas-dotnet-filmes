﻿using AutoMapper;
using Ioasys.Business.Dtos;
using Ioasys.Business.Models;

namespace Ioasys.Api.Configuration
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<User, DtoUserRegistration>().ReverseMap();
            CreateMap<User, DtoUser>().ReverseMap();

            CreateMap<Movie, DtoMovie>().ReverseMap();
        }
    }
}
