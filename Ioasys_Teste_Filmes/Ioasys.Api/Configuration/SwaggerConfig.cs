﻿using Ioasys.Api.Attributes;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.OpenApi.Models;
using System.IO;

namespace Ioasys.Api.Configuration
{
	public static class SwaggerConfig
	{
		public static IServiceCollection ResolveSwagger(this IServiceCollection services)
		{
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new OpenApiInfo
				{
					Title = "Teste Ioasys API Filmes",
					Version = "v1",
					Description = "API teste desenvolvida para ser consumida pelo IMDB",
					Contact = new OpenApiContact
					{
						Name = "Thiago Miranda",
						Email = "tf.arcanjo@gmail.com"
					},
				});

				c.AddSecurityDefinition("bearer", new OpenApiSecurityScheme
				{
					Name = "Authorization",
					Type = SecuritySchemeType.ApiKey,
					Scheme = "bearer",
					BearerFormat = "JWT",
					In = ParameterLocation.Header,
					Description = "Autorização JWT API",
				});

				c.OperationFilter<AuthOperationFilter>();

				string caminhoAplicacao = PlatformServices.Default.Application.ApplicationBasePath;
				string nomeAplicacao = PlatformServices.Default.Application.ApplicationName;
				string caminhoXmlDoc = Path.Combine(caminhoAplicacao, $"{nomeAplicacao}.xml");
				c.IncludeXmlComments(caminhoXmlDoc);

			});

			return services;
		}
	}
}
