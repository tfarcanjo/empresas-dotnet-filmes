﻿using Ioasys.Business.Interfaces;
using Ioasys.Business.Services;
using Ioasys.Business.Services.Interfaces;
using Ioasys.Data.Context;
using Ioasys.Data.Repositories;
using Microsoft.Extensions.DependencyInjection;

namespace Ioasys.Api.Configuration
{
    public static class DependencyInjectionConfig
    {
        public static IServiceCollection ResolveDependencies(this IServiceCollection services)
        {
            services.AddScoped<MoviesDbContext>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IMovieRepository, MovieRepository>();
            services.AddScoped<IActorRepository, ActorRepository>();
            services.AddScoped<IMovieActorRepository, MovieActorRepository>();

            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IMovieService, MovieService>();

            return services;
        }
    }
}
